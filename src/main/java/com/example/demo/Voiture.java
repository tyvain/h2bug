package com.example.demo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "SCOLARITE.VOITURE")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Voiture {
    @Id
    private String id;
}
