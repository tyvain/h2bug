package com.example.demo;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Immutable;
import org.hibernate.annotations.Subselect;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;

@Entity
@Immutable
@Getter
@NoArgsConstructor
@EqualsAndHashCode
@Subselect("SELECT DISTINCT ID FROM SCOLARITE.VOITURE")
public class VoitureView implements Serializable {
    @Id
    String id;
}