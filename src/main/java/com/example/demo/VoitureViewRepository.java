package com.example.demo;

import org.springframework.data.jpa.repository.JpaRepository;

public interface VoitureViewRepository extends JpaRepository<VoitureView, String> {

}
