package com.example.demo;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase
public class DemoApplicationTests {

	@Autowired
	private VoitureRepository voitureRepository;

	@Autowired
	VoitureViewRepository voitureViewRepository;

	@Test
	public void testSpringContext() throws Exception {
		Voiture voiture1 = new Voiture("1");
		Voiture voiture2 = new Voiture("2");
		voitureRepository.save(voiture1);
		voitureRepository.save(voiture2);
		System.out.println("all -> " + voitureRepository.findAll().size());
		System.out.println("findById -> " + voitureRepository.findById("1"));
		// error is just bellow
		System.out.println("view all -> " + voitureViewRepository.findAll());
	}

}
